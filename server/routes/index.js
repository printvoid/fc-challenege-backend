var express = require('express');
var router = express.Router();
var fcController = require('../controller/fc.controller')

/* GET home page. */
router.get('/', function(req, res, next) {
  fcController.list(req, res);
});


/* GET All Keys in DB. */
router.get('/allKeys', function(req, res, next) {
  fcController.getAllKeys(req, res);
});

/* GET a Specific Key in the DB. */
router.get('/allKeys/:id', function(req, res, next) {
  fcController.getKeyWithId(req, res);
});

/* POST a Specific Key in the DB. */
router.post('/allKeys/:id', function(req, res) {
  fcController.setKeyWithId(req, res);
})

/* Delete a Specific Key in the DB. */
router.delete('/allKeys/:id', function(req, res) {
  fcController.deleteKey(req, res);
})

/* Delete All Keys in the DB. */
router.delete('/allKeys', function(req, res) {
  fcController.deleteAllKeys(req, res);
})


module.exports = router;
