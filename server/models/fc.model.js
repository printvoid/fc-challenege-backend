var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var fcCacheKeysSchema = new Schema({
    key: {
		type: String
	},
	ttl: {
		type: Number,
		default: 86400
	},
	content: {
		type: String,
		default: (Math.random().toString(36).substring(7))
	}
});


module.exports = mongoose.model('fcSchema', fcCacheKeysSchema)