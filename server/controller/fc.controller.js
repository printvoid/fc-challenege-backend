var path = require('path');
var Promise = require('bluebird');
var fcCacheKeys = require('../models/fc.model');

exports.list = function(req, res) {
    var query = fcCacheKeys.find();
    query.sort()
            .exec(function(err, results) {
                res.send(200, {cacheItems : results})
            })
}

exports.getAllKeys = function(req, res) {
    var query = fcCacheKeys.find();
      query.sort()
            .exec(function(err, results) {
                res.send(200, {cacheItems : results})
            })
}

exports.getKeyWithId = Promise.coroutine(function*(req, res) {
        var entry = new fcCacheKeys({
            key: req.params.id,
            content: req.body.content
        });
        try {
         var cache = yield fcCacheKeys.findOne({key: req.params.id}).exec();
           console.log('The cache is', cache) 
            if (cache.isNew) {
                entry.save(function(err, results) {
                    if(err) {
                        var errMsg = 'Sorry but there was an error saving your details,' + err;
                        res.send(500, {title: 'Error addeding a Cache in the DB', message: errMsg})    
                    }
                    else {
                        console.log('I saved entry as', entry);
                        res.send(200, {title: 'Successfully added a Cache in the DB', message: 'Cache Hit', cache: results}) 
                    }
                });
                res.send(200, { message: 'Cache MISSS', cache: results}) 
            } else {
                message = 'Cache hit';
                res.send(200, {message: 'Cache Hit', cache: results}) 
            }
        } catch (err) {
            entry.save(); //TODO: //This is a hack here for now..., for the first insert 
            res.status(200).send("I have added the entry to DB");
        }
        finally {
            res.status(400).send("Something bad happenend: There needs to be a key before you check IsNew", err);
        }
})

exports.setKeyWithId = Promise.coroutine(function*(req, res) {
    var cache = yield fcCacheKeys.findOne({key: req.params.id}).exec();
    if (cache) {
        cache.content = req.body.content;
        yield cache.save();
      } else {
        cache = yield fcCacheKeys.create({
          key: req.params.id,
          content: req.body.content
        });
      }
         res.status(200).send({
         message: 'Content added to db.'
    });
})

/* Delete Function, deletes DB entry */
exports.deleteKey = function(req, res) {
    var query = fcCacheKeys.remove({
        key: req.params.id
    }).exec();
    res.status(200).send({
        message: 'Key ' + req.params.id + ' has been deleted successfully'
    });
}

/* Delete Function, deletes ALL DB entries */
exports.deleteAllKeys = function(req, res) {
    var query = fcCacheKeys.find();
        query.sort()
                .remove(function(err, results) {
                    if(err) throw new Error(500, {message: 'Error Deleting ther Entries'})
                    res.send(200, {message : 'All DB entries successfully cleaned...'})
                })
}