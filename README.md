# A brief Overview

## This is a Fashion Cloud repository for backend challenge.

## This repo follows THE MEVN Stack pattern architecture, Front end Vue boiler plate code is added to extend the backend

### Steps for Frontend
1. Goto client Folder, <br>
    a: npm install
    b: npm run dev    

## Steps for Backend

1. Goto server Folder, <br>
    a: npm install
    b: node www/bin

Since this repo is only about the backend or server code, you can just run the code on postman, provided you have an instance of mongoose running.

# Thank You

